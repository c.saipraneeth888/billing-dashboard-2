const express = require('express');
const router = express.Router();
const {inserttodb,healthcheck}= require('../lib/db-helper.js')
const {verifyschema}= require('../lib/verifyschema.js')
router.post('/log', (req, res) => {
  body=req.body
  verifyschema(body)
  .then(()=>{
    inserttodb(body)
    .then((result)=>{
      res.send(result)
    })
    .catch((err)=>{
      console.log(err)
      res.status(400)
      res.send(err)
    })
  })
  .catch((err)=>{
    returnErr=[]
    err.forEach(element => {
      returnErr.push({"errorMessage":element["message"]})
    });
    res.status(400)
    res.send(JSON.stringify(returnErr))
  })
});

router.get('/healthcheck',(req,res)=>{
  healthcheck()
  .then((result)=>{
    res.send(result)
  })
  .catch((err)=>{
    res.status(500)
    res.send(err)
  })
})
module.exports = router;
