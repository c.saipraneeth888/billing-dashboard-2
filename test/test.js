var assert = require('assert');
const uuidv4 = require('uuid/v4');
const {inserttodb,select} = require('../lib/db-helper.js')
const {verifyschema}= require('../lib/verifyschema.js')
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var should = chai.should();
chai.use(chaiAsPromised);

describe('check database', function() {
  databasedata={
    "timeStamp":"1212/12/12",
    "statusCode":200,
    "errorMessage":"noneerror",
    "result":{"1":"199"},
    "requestId":uuidv4(),
    "product":"product",
    "url":"localhost",
    "requestParams":{"1":1},
    "appId":"saipraneeth",
    "host":"saipraneeth",
    "referenceId":"saipraneeth",
    "transactionId":"saipraneeth",
    "clientData":{"1":1},
    "responseTime":1,
    }
  it('should be fulfilled when the value is inserted', async function() {
    return inserttodb(databasedata).should.be.fulfilled;
  });

  it('check if data is inserted',async()=>{
    assert.equal(await select(databasedata["requestId"]),databasedata["requestId"])
  })

  it('should be rejected when same value is inserted', function() {
    inserttodb(databasedata).should.be.rejected;
  });
});

describe('check validationSchema', function() {
  data={
    "timeStamp":"1212/12/12",
    "statusCode":200,
    "errorMessage":"noneerror",
    "result":{"1":"199"},
    "requestId":uuidv4(),
    "product":"product",
    "url":"localhost",
    "requestParams":{"1":1},
    "appId":"saipraneeth",
    "host":"saipraneeth",
    "referenceId":"saipraneeth",
    "transactionId":"saipraneeth",
    "clientData":{"1":1},
    "responseTime":1,
  }
  it('should be fulfilled when the value is inserted', function() {
    return verifyschema(data).should.be.fulfilled;
  });

  it('should be rejected when the value is validated wrong timestamp', function() {
    params=JSON.parse(JSON.stringify(data))
    params["timeStamp"]="500/500/500"
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong statuscode', function() {
    params=JSON.parse(JSON.stringify(data))
    params["statusCode"]="string"
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong errormessage', function() {
    params=JSON.parse(JSON.stringify(data))
    params["errorMessage"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong result', function() {
    params=JSON.parse(JSON.stringify(data))
    params["result"]=1
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong requestid', function() {
    params=JSON.parse(JSON.stringify(data))
    params["requestId"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong product', function() {
    params=JSON.parse(JSON.stringify(data))
    params["product"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong url', function() {
    params=JSON.parse(JSON.stringify(data))
    params["url"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong requestparams', function() {
    params=JSON.parse(JSON.stringify(data))
    params["requestParams"]="string"
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong appid', function() {
    params=JSON.parse(JSON.stringify(data))
    params["appId"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong host', function() {
    params=JSON.parse(JSON.stringify(data))
    params["host"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong referenceid', function() {
    params=JSON.parse(JSON.stringify(data))
    params["referenceId"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong appid', function() {
    params=JSON.parse(JSON.stringify(data))
    params["transactionId"]={1:1}
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong clientdata', function() {
    params=JSON.parse(JSON.stringify(data))
    params["clientData"]="string"
    return verifyschema(params).should.be.rejected
  });

  it('should be rejected when the value is validated wrong responsetime', function() {
    params=JSON.parse(JSON.stringify(data))
    params["responseTime"]={1:1}
    return verifyschema(params).should.be.rejected
  });
});

