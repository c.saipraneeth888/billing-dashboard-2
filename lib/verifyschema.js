const Joi = require("@hapi/joi").extend(require('@hapi/joi-date'))
const schema = Joi.object().options({ abortEarly: false }).keys({
    timeStamp: Joi.date().format('YYYY/MM/DD').utc().required(),
    statusCode: Joi.number().required(),
    errorMessage: Joi.string(),
    result: Joi.object(),
    requestId: Joi.string(),
    product: Joi.string().required(),
    url: Joi.string().required(),
    requestParams: Joi.object().required(),
    appId: Joi.string().required(),
    host: Joi.string().required(),
    referenceId: Joi.string(),
    transactionId: Joi.string(),
    responseTime: Joi.number().required(),
    clientData: Joi.object().required()
})
function verifyschema(data){
    return new Promise((resolve,reject)=>{
        errorInSchema=schema.validate(data)["error"]
        if (errorInSchema){
            console.log("err",errorInSchema)
            reject(errorInSchema["details"])
        }
        resolve("done")
    })
}
// data1={
// 	"timeStamp":"12/12",
// 	"statusCode":"sai",
// 	"errorMessage":"error",
// 	"result":{"1":"199"},
// 	"requestId":"25",
// 	"product":"product",
// 	"url":"localhost",
// 	"requestParams":{"1":1},
// 	"appId":"saipraneeth",
// 	"host":"saipraneeth",
// 	"referenceId":"saipraneeth",
// 	"transactionId":"saipraneeth",
// 	"clientData":{"1":1},
// 	"responseTime":1
// }
// verifyschema(data1)
// .then((data)=>{
//     console.log("data",data)
// })
// .catch((err)=>{
//     console.log("err",err)
// })
exports.verifyschema=verifyschema