const {Client} = require('pg');
const {config} = require('../config.js')

const client = new Client({
  user: config.user || "saipraneeth",
  host: config.host || "localhost",
  database: config.database || "dashboard",
  password: config.password || "saipraneeth",
  port: config.postgresPort || "5432",
  ssl: true,
});
client.connect()
keys=["timeStamp","statusCode","errorMessage","result","requestId","product","url","requestParams","appId","host","referenceId","transactionId","clientData","responseTime"]
values=[]
function inserttodb(data){
    return new Promise((resolve,reject)=>{
        temp=[]
        for (let i=0;i<keys.length;i++){
            isjson=false
            check=["result","requestParams","clientData"]
            check.forEach(function (val){
                if (val===keys[i]){
                    isjson=true
                }
            })
            if (isjson){
                temp.push(JSON.stringify(data[keys[i]]||null))
            }else{
                temp.push(data[keys[i]]||null)
            }
        }
        values.push(temp)
        resolve({status:"done"})
    })
}

function uploadtodb(){
    if (values.length>0){
        query=`insert into details (${keys.join(',')}) values`
        values.forEach(list => {
            query+=` ('${list.join("','")}'),`
        });
        query=query.substr(0,query.length-1)+';'
        values=[]
        console.log(query)
        client.query(query,(err)=>{
            if (err){
                console.log(err)
            }
        })
    }
    console.log("done")
}

setInterval(uploadtodb,3000)
function select(requestid){
    return new Promise((resolve)=>{
        client.query(`select requestid from details where requestid='${requestid}';`,(err,res)=>{
            if (res["rows"].length==0){
                reject("not fond")
            }
            resolve(res["rows"][0]["requestid"])
        })
    })
}

function healthcheck(){
    return new Promise((resolve,reject)=>{
        if (client.connection.stream._parent !== null){
            resolve({status:"connected to DB"})
        }else{
            reject({status:"unable to connect to DB"})
        }
    })
}
exports.inserttodb=inserttodb
exports.select=select
exports.healthcheck=healthcheck