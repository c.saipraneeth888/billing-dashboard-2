const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');
const {config} = require('./config.js')
const app = express();
app.use(bodyParser.json());
app.use('/', routes);
app.listen(config.port|| 8080, () => { console.log(`started on ${config.port|| 8080}`); });